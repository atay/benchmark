#!/bin/bash

cp -rf $(pwd)/alice $(pwd)/workdir/alice_${SLURMD_NODENAME}_${SLURM_LOCALID}
cd $(pwd)/workdir/alice_${SLURMD_NODENAME}_${SLURM_LOCALID}
hostname
ts=$(date +%s%N)
singularity exec -B /cvmfs -B /scratch /scratch/compeng/atay/alice.sif /cvmfs/alice.cern.ch/bin/alienv setenv AliPhysics/vAN-20201018_ROOT6-1 -c aliroot runAnalysis.C
tt=$((($(date +%s%N) - $ts)/1000000000))
echo "$tt second"

hostname
cd ../
rm -rf alice_${SLURMD_NODENAME}_${SLURM_LOCALID}
exit
