# Benchmark for the Verbund project

## Description
A parametrized package to submit a analysis-benchmark slurm job with caching to Goethe-HLR. 

## Usage
- git clone https://git.gsi.de/atay/Benchmark.git

-- Starting caching server. Set the variables in caching/parameters bash file
- cd Benchmark/caching
- ./benchmark -m dcotf
    - -m/--mode             dcotf, xcache or xcache_dca
    - -h/--help             displays help

-- Starting clients. Set variables in client/client bash file
- cd ../client
- sbatch client

## Procedure explanation
- "benchmark" sources "parameters" to and crates xrootd config files.
    - It starts xrootd servers on the worker node
    - output, logs and xrootd configs can be found respective folders
    - after starting the caching server, please first fetch data from external data server so that client can get fetch from local filesystem. It is best to wait until the caching server detaches the file
        - check logs/<mode>_xrootd.log. If you see Detach() function is called in the history then you can start clients.
- "client" starts clients
    - output can be found in output/${SLURM_JOB_NAME}_${SLURM_JOB_ID}.

## Notes
- Please don't forget to cancel caching server job after you finish since it will run until it exceeds the time limit
    - scancel -n "job_name" or
    - scancel -w "node_name" or
    - scancel "job_id"
- You can check availability of reserved nodes
    - squeue -w node22-[008-011]